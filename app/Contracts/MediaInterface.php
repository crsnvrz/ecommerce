<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface MediaInterface {
    public function savePhoto($cover_image);
    public function saveSize($cover_image, $size);
    public function saveMedia($name, $post, $size);
}