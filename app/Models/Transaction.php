<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Transaction extends Model
{
    protected $fillable = [
        'name',
        'is_served',
        'cash',
        'change',
        'total_amount',
        'discount',
        'branch_id'
    ];

    protected $with = [ 'orders' ];

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->toDayDateTimeString();
    }
}
