<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('branch_id')->nullable();

            $table->foreign('branch_id')->references('id')->on('branches');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('branch_id');

            $table->foreign('branch_id')->references('id')->on('branches');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedInteger('branch_id');

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('transactions');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('products');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('users');
        });

        Schema::dropIfExists('branches');
    }
}
