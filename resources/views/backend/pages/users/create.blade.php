@extends('backend.layouts.app')

@section('content')
<div class="card">
<div class="card-header">
    <b>Add User</b>
</div>
<div class="card-body">
    {{ Form::open(['route' => 'users.store', 'method' => 'POST', "enctype" => "multipart/form-data", 'id' => 'users-form']) }}
    <div class="col-md-5 mx-auto">
        <div class="form-group">
            {{ Form::label('first_name', 'First Name', ['class' => 'control-label']) }}
            {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('last_name', 'Last Name', ['class' => 'control-label']) }}
            {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', null, ['class' => 'control-label']) }}
            {{ Form::text('email', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', null, ['class' => 'control-label']) }}
            {{ Form::password('password', array('class'=>'form-control') ) }}
        </div>
        <div class="form-group">
            {{ Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label']) }}
            {{ Form::password('password_confirmation', array('class'=>'form-control') ) }}
        </div>
        <div class="form-group">
            {{ Form::label('role', 'User Role', ['class' => 'control-label']) }}
            {{ Form::select('role[]', $roles, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {!! Form::label('branch_id', 'Branch') !!}
            {!! Form::select('branch_id', $branches, null, ['class' => 'form-control', 'required']) !!}
        </div>
        <center><button class="btn btn-lg btn-success">Submit</button></center>
    </div>
    {!! Form::close() !!}
</div>
</div>
@endsection