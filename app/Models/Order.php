<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;
use App\Models\Product;

class Order extends Model
{
    protected $filalble =[
        'product_id',
        'quantity',
        'price',
        'size'
    ];

    protected $with = [ 'product' ];

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
