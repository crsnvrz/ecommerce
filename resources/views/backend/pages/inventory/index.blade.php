@extends('backend.layouts.app')

@section('css')
    <style>
        thead > tr > td{
            font-weight: bold;
        }
        tbody > tr > td > img{
            width: 40px;;
        }
    </style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">Inventory</div>
    <div class="card-body">
        <div class="pull-right">
            {!! Form::open(['route' => 'products.index', 'method' => 'get', 'enctype' => 'multipart/form-data', 'id' => 'prodForm']) !!}
            {!! Form::hidden('category_id', null, ['class' => 'form-control', 'readonly']) !!}
            {!! Form::close() !!}
            <select id="categories" class="form-control" v-on:change="chooseCategory">
                @foreach( $categories as $category )
                        @if($category->id == $selected_category)
                            <option value="{{ $category->id }}" selected> {{ $category->name }} </option>
                        @else
                            <option value="{{ $category->id }}"> {{ $category->name }} </option>
                        @endif
                @endforeach
            </select>
        </div>
        <br><br>
       @if($products->isNotEmpty())
       <div class="table">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td></td>
                        <td>Name</td>
                        <td>Quantity (Small)</td>
                        <td>Quantity (Medium)</td>
                        <td>Quantity (Large)</td>
                        <td>Category</td>
                        <td>Branch</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td><img src="{{ $product->getUrl(50) }}" alt=""></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ $product->quantity_medium }}</td>
                        <td>{{ $product->quantity_large }}</td>
                        <td>{{ $product->product_category->name }}</td>
                        <td>{{ $product->branch->name }}</td>
                        <td>
                            <a href="{{route('inventory.edit', $product->id)}}"><button type="button" class="btn btn-info">Update</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $products->appends(["category_id" => $selected_category])->links() }}
        @else
        <center>
            <h2>No products yet...</h2>
        </center>
        @endif
    </div>
</div> 
@endsection
@section('js')
    <script src="{{ asset('js/products.js') }}"></script>
@endsection