<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;

class InventoryController extends Controller
{
    private $category = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->has('category_id')){
            $this->category = request()->get('category_id');
        }

        $products = Product::whereHas('product_category', function($query){
            $query->where('id', $this->category);
        })
        ->paginate(5);

        $selected_category = $this->category;

        $categories = ProductCategory::all(); 

        return view('backend.pages.inventory.index', compact('products', 'categories', 'selected_category'));

        // $products = Product::all();
        // return view('backend.pages.inventory.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('backend.pages.inventory.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'quantity' => 'required|integer',
            'quantity_medium' => 'nullable|integer',
            'quantity_large' => 'nullable|integer'
        ]);

        $product->fill($request->all());
        $product->save();
        
        return redirect('/inventory')->with('success', 'Product inventory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
