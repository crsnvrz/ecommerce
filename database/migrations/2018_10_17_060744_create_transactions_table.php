<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_served')->nullable();
            $table->integer('cash');
            $table->integer('change');
            $table->integer('total_amount');
            $table->integer('discount')->nullable();
            $table->timestamps();
        });

        Schema::create('order_transaction', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('transaction_id');

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('order_transaction');
        Schema::dropIfExists('transactions');
    }
}
