<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Media;
use App\Models\Branch;
use App\Contracts\MediaInterface;
use App\Http\Requests\ProductRequest;
use Image;

class ProductController extends Controller
{
    protected $sizes  = [80, 160, 240, 480, 800, 1080];
    protected $coverImage, $blog, $media;

    private $category = 1;

    public function __construct(MediaInterface $media){
        $this->media = $media;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->has('category_id')){
            $this->category = request()->get('category_id');
        }

        $products = Product::whereHas('product_category', function($query){
            $query->where('id', $this->category);
        })
        ->paginate(5);

        $selected_category = $this->category;

        $categories = ProductCategory::all(); 
        
        return view('backend.pages.products.index', compact('products', 'categories', 'selected_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ProductCategory::all()->pluck('name', 'id');
        $branches = Branch::all()->pluck('name', 'id');
        return view('backend.pages.products.create', compact('categories', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = new Product($request->all());
        $product->save();

        if($request->hasFile('cover_image')){
            $this->coverImage = $request->file('cover_image');
            $this->blog = $product;
            $fileName = $this->media->savePhoto($this->coverImage);
            $width = Image::make($this->coverImage)->width();
            $this->media->saveMedia($fileName, $this->blog, $width);

            $this->sizes = collect( $this->sizes);
            $this->sizes->each(function($size){
                $resizedImg = $this->media->saveSize($this->coverImage, $size);
                $this->media->saveMedia($resizedImg, $this->blog, $size);
            });
        } else {
            $fileName = 'noimage.png';
            $this->media->saveMedia($fileName, $product, 80);
        }

        return redirect('/products')->with('success', 'Product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = ProductCategory::all()->pluck('name', 'id');
        $branches = Branch::all()->pluck('name', 'id');
        return view('backend.pages.products.edit', compact('product', 'categories', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->fill($request->all());
        
        $this->blog = $product;

        $product->save();

        $images = $product->media()->where('mediable_id', $product->id)->pluck('name');

        if($request->hasFile('cover_image')){    
            $images->each(function($image){
                if($image != 'noimage.png'){            
                    Storage::delete('public/cover_images/'.$image);
                }
                $this->blog->media()->delete();
            });

            $this->coverImage = $request->file('cover_image');
            $this->blog = $product;
            $fileName = $this->media->savePhoto($this->coverImage);
            $width = Image::make($this->coverImage)->width();
            $this->media->saveMedia($fileName, $this->blog, $width);

            $this->sizes = collect($this->sizes);
            $this->sizes->each(function($size){
                $resizedImg = $media->saveSize($this->coverImage, $size);
                $this->media->saveMedia($resizedImg, $this->blog, $size);
            });
        }

        return redirect('/products')->with('success', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $images = $product->media()->where('mediable_id', $product->id)->pluck('name');

        $images->each(function($image){
            if($image != 'noimage.png'){            
                Storage::delete('public/cover_images/'.$image);
            }
        });
        
        try{
            $product->media()->delete();
            $product->delete();
            
            return redirect('/products')->with('success', 'Product Removed');

        }catch(Exception $exception)
        {
            $errormsg = 'Sorry, unable to delete product:'. $errormsg;
            return redirect('/products')->with('error', $errormsg);
        } 
    }
}
