@extends('backend.layouts.app')

@section('css')
    <style>
        thead > tr > td{
            font-weight: bold;
        }
        tbody > tr > td > img{
            width: 40px;;
        }
    </style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">Branches</div>
    <div class="card-body">
        <a href="{{ route('branches.create') }}" class="btn btn-primary" onclick="blur()">Add Branch</a>
       <br><br>
       @if($branches->isNotEmpty())
       <div class="table">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Branch Name</td>
                        <td>Address</td>
                        <td><center>Address</center></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($branches as $branch)
                    <tr>
                        <td>{{ $branch->name }}</td>
                        <td>{{ $branch->address }}</td>
                        <td>
                            <a href="{{ route('branches.edit', $branch->id)}}"><button type="button" class="btn btn-info">Edit</button></a>
                            {!!Form::open(['route' => ['branches.destroy', $branch->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=> 'return confirm("Are you sure you want to delete?")'])}}
                            {!!Form::close()!!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <center>
            <h2>No branch yet...</h2>
        </center>
        @endif
    </div>
</div> 
@endsection
