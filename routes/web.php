<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.pages.home.index');
})->name('homepage');

Route::get('about', function () {
    return view('frontend.pages.about.index');
})->name('about');

Route::get('items', function () {
    return view('frontend.pages.products.index');
})->name('product');

Route::get('store', function () {
    return view('frontend.pages.store.index');
})->name('store');

Auth::routes();

Route::get('home', 'Backend\HomeController@index')->name('home');

Route::resource('cashier', 'Backend\CashierController')->middleware('auth');

Route::resource('products', 'Backend\ProductController')->middleware('auth');

Route::resource('inventory', 'Backend\InventoryController')->middleware('auth')->parameters(['inventory'=>'product']);

Route::resource('categories', 'Backend\ProductCategoryController')->middleware('auth');

Route::resource('users', 'Backend\UserController')->middleware('auth');

Route::resource('orders', 'Backend\OrderController')->middleware('auth');

Route::resource('branches', 'Backend\BranchController')->middleware('auth');

Route::resource('transactions', 'Backend\TransactionController')->middleware('auth');

Route::get('users-search', 'Backend\UserController@index')->name('users.search');