<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\ProductCategory;

class ProductController extends Controller
{
    public function getItems(){

        $products = Product::whereHas('product_category', function($query){
            $query->where('id', request()->category_id); 
        })
        ->get();
        
        $categories = ProductCategory::all();

        $data = [
            'products' => $products,
            'categories' => $categories
        ];

        return response()->json( $data );
    }

    public function saveOrders(){
        $transact = new Transaction();
        $transact->name = 'Customer';
        $transact->fill(request()->all());
        $transact->save();

        foreach(request()->orders as $order){
            $purchase = new Order();
            $purchase->product_id = $order['id'];
            $purchase->quantity = $order['order_quantity'];
            $purchase->price = $order['order_price'];
            $purchase->size = $order['order_size'];
            $purchase->save();

            $product = Product::find($order['id']);
            if($order['order_size'] == 'small'){
                $product->quantity =  $product->quantity - $order['order_quantity'];
                $product->update();
            }
            else if($order['order_size'] == 'medium'){
                $product->quantity_medium =  $product->quantity_medium - $order['order_quantity'];
                $product->update();
            }
            else{
                $product->quantity_large =  $product->quantity_large - $order['order_quantity'];
                $product->update();
            }

            $transact->orders()->attach($purchase->id);
        }

        $transact->load('orders');

        return response()->json($transact);
    }

    public function getCategory(){
        $category = ProductCategory::find(request()->id);

        return response()->json($category);
    }
}
