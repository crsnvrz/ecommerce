let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/custom/products.js', 'public/js')
   .js('resources/assets/js/custom/cashier.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/sidebar/simple-sidebar.scss', 'public/css/sidebar')
   .sass('resources/assets/sass/style.scss', 'public/css')
   .sass('resources/assets/sass/business-casual.scss', 'public/css');

mix.copyDirectory('resources/assets/images', 'public/assets/images');