<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Helper\Utilities;
use App\Contracts\MediaInterface;
use App\Models\Media;
use Image;

class MediaRepository implements MediaInterface{

    /*
     * Method for saving the original photo 
     */
    public function savePhoto($cover_image){

        $filenameWithExt = $cover_image->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $cover_image->getClientOriginalExtension();
        $fileNameToStore= $filename.'_orig_'.time().'.'.$extension;
        $path = $cover_image->storeAs('public/cover_images', $fileNameToStore);

        return $fileNameToStore;
    }
    
    /*
     * Method for saving different photo sizes
     */
    public function saveSize($cover_image, $size){
        
        $filenameWithExt = $cover_image->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $cover_image->getClientOriginalExtension();

        $img = Image::make($cover_image->getRealPath());
        $img->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $croppedImage = $filename.'_'.$size.'_'.time().'.'.$extension;
        $tempFilePath = storage_path('app/public/cover_images/' . $croppedImage);  

        $img->save($tempFilePath);   

        return $croppedImage;
    }

    /*
     * Method for saving media
     */
    public function saveMedia($name, $post, $size){    
        $media = new Media([
            'name' => $name,
            'description' => 'cover_image',
            'width' => $size
        ]);

        $post->media()->save($media);
    }
}