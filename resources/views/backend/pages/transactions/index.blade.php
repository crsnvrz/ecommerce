@extends('backend.layouts.app')

@section('css')
    <style>
        thead > tr > td{
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">Transactions</div>
    <div class="card-body">
       @if($transactions->isNotEmpty())
       <div class="table">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Transaction Date</td>
                        <td>Total Amount</td>
                        <td>Cash</td>
                        <td>Change</td>
                        <td>Discount</td>
                        <td>Branch</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->created_at }}</td>
                        <td>{{ $transaction->total_amount }}</td>
                        <td>{{ $transaction->cash }}</td>
                        <td>{{ $transaction->change }}</td>
                        <td>{{ $transaction->discount }}</td>
                        <td>{{ $transaction->branch->name }}</td>
                        <td>
                            <a href="{{ route('transactions.show', $transaction->id) }}"><button type="button" class="btn btn-info">View</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $transactions->links() }}
        @else
        <center>
            <h2>No transactions yet...</h2>
        </center>
        @endif
    </div>
</div> 
@endsection