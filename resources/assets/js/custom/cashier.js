import Products from '../components/ProductComponent'
import ProductCategory from '../components/ProductCategoryComponent'
import printThis from 'print-this'

new Vue({
    el: '#app',
    data: {
        categories: null,
        items: null,
        orders: [],
        order_quantity: null,
        total_amount: 0,
        products: [],
        buttons: [],
        is_sizes: false,
        price: null,
        order_size: null,
        selected_quantity: null,
        show_close_modal: false,
        cash: null,
        change: null,
        is_change: false,
        receipt: null,
        transaction: null,
        is_discount: false,
        discount: null,
        final_amount: null,
        branch_id: transact.branch_id,
    },
    created() {
        this.getItems()
    },
    methods:{
        getItems(id){
            if(!id){
                id = 1
            }
            axios.post('/api/getItems', {"category_id": id} )
                .then(response => {
               this.categories = response.data.categories
               this.items = response.data.products
            })
        },
        removeOrder(key){
            this.total_amount = this.total_amount - this.orders[key].order_amount
            this.final_amount = this.total_amount
            this.orders.splice(key, 1)
            this.buttons.splice(key, 1)

            if(this.is_discount){
                this.is_discount = false
                this.addDiscount()
            }
            
            if(this.orders.length == 0){
                this.is_discount = false
            }
        },
        addQuantity(){
            if(this.order_quantity == 0 || this.order_quantity == null){
                alert("Please add quantity")
                return false
            }

            if(this.order_quantity > this.selected_quantity){
                alert("You only have " + this.selected_quantity + " remaining items for this product.")
                this.show_close_modal = true
                return false
            }

            if(this.selected_quantity == 0 || this.selected_quantity == null){
                alert("Sorry, no more available stocks for this product.")
                this.show_close_modal = true
                return false
            }

            if(this.products.product_category.with_sizes){
                let size = $("select[name='sizes'] option:selected").val()
                this.order_size = size
                if (size == 'small'){
                    if(this.buttons.includes(this.products.id + this.products.price)){
                        alert("This product was already added on the order list.")
                        return false
                    }
                }
                else if (size == 'medium'){
                    if(this.buttons.includes(this.products.id + this.products.price_medium)){
                        alert("This product was already added on the order list.")
                        return false
                    }
                }
                else{
                    if(this.buttons.includes(this.products.id + this.products.price_large)){
                        alert("This product was already added on the order list.")
                        return false
                    }
                }
            }

            this.orders.push(this.products)
            
            if(this.products.product_category.with_sizes){
                let size = $("select[name='sizes'] option:selected").val()
                this.order_size = size
                if (size == 'small'){
                    this.buttons.push(this.products.id + this.products.price)
                }
                else if (size == 'medium'){
                    this.buttons.push(this.products.id + this.products.price_medium)
                }
                else{
                    this.buttons.push(this.products.id + this.products.price_large)
                }
            }
            else{
                this.buttons.push(this.products.id)
            }
            // this.buttons.push(this.products.id)
            let key = this.orders.length - 1
            if(this.orders.length == 0) key = 0

            Object.assign(this.orders[key], {order_quantity : this.order_quantity})
            Object.assign(this.orders[key], {order_amount: this.order_quantity * this.price})
            Object.assign(this.orders[key], {order_price: this.price})
            Object.assign(this.orders[key], {order_size: this.order_size})
            this.total_amount = this.total_amount + this.orders[key].order_amount
            this.final_amount = this.total_amount
            $("#cashier-modal").modal("hide")
            this.is_sizes = false
            this.order_quantity = null
            this.price = null
            this.selected_quantity = null

            if(this.is_discount){
                this.is_discount = false
                this.addDiscount()
            }
        },
        setPrice(event){
            let size = $(event.currentTarget).val()
            this.order_size = size
            if (size == 'small'){
                this.price = this.products.price
                this.selected_quantity = this.products.quantity
            }
            else if (size == 'medium'){
                this.price = this.products.price_medium
                this.selected_quantity = this.products.quantity_medium
            }
            else{
                this.price = this.products.price_large
                this.selected_quantity = this.products.quantity_large
            }
        },
        closeModal(){
            this.show_close_modal = false
            this.price = null
            this.selected_quantity = null
            this.order_quantity = null
            this.is_sizes = false
        },
        checkout(event){
            
            if(this.final_amount > this.cash){
                alert("Invalid cash input.")
                return false
            }

            let params = {
                'orders' : this.orders,
                'cash' : this.cash,
                'change' : this.change,
                'total_amount': this.final_amount,
                'discount': this.discount,
                'branch_id': this.branch_id
            }

            $(event.currentTarget).attr("disabled", true)

            axios.post('/api/saveOrders', params )
                .then(response => {
                this.receipt = response.data.orders
                this.transaction = response.data
                $("#receipt-modal").modal()
            })
        },
        calcChange(){
            this.is_change = true
            this.change = parseInt(this.cash) - this.final_amount
        },
        printReceipt(){
            $(".printDiv").printThis()
        },
        addDiscount(){
            if(this.is_discount){
                this.final_amount = this.final_amount + this.discount
                this.is_discount = false
                return false
            }

            this.discount = this.final_amount * 0.2
            this.final_amount = this.final_amount - this.discount
            this.is_discount = true
        }
    },
    components: {
        products : Products,
        categories : ProductCategory
    }
})