<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class ProductCategory extends Model
{
    protected $table = 'product_category';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;
    
    protected $fillable = ['name', 'with_sizes'];
    
    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function getWithSizesAttribute($value){
        return (bool) $value;
    }
}
