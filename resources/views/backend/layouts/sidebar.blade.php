<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    SALE
                </a>
            </li>
            <li>
                <a href="/home"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('cashier.index') }}"><i class="fa fa-calculator"></i> Cashier</a>
            </li>
            <li class="sidebar-brand">
                <a href="#">
                    Products
                </a>
            </li>
            <li>
                <a href="{{ route('products.index') }}"><i class="fa fa-shopping-basket"></i> Items</a>
            </li>
            <li>
                <a href="{{ route('categories.index') }}"><i class="fa fa-tags"></i> Categories</a>
            </li>
            <li>
                <a href="{{ route('inventory.index') }}"><i class="fa fa-cubes"></i> Inventory</a>
            </li>
            <li class="sidebar-brand">
                <a href="#">
                    Reports
                </a>
            </li>
            <li>
                <a href="{{ route('branches.index') }}"><i class="fa fa-th-list"></i> Branches</a>
            </li>
            <li>
                <a href="{{ route('transactions.index') }}"><i class="fa fa-bar-chart-o"></i> Transaction</a>
            </li>
            <li>
                <a href="{{ route('users.index') }}"><i class="fa fa-user"></i> User Account</a>
            </li>
        </ul>
    </div>
    <div id="page-content-wrapper">
        @include('inc.messages')
        @yield('content')
    </div>
</div>