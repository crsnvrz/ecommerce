<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public $primaryKey = 'id';

    public $timestamps = true;

    protected $with = ['product_category'];

    protected $fillable = [
        'name', 
        'product_category_id', 
        'price', 
        'price_medium', 
        'price_large',
        'quantity',
        'quantity_medium',
        'quantity_large',
        'branch_id'
    ];

    protected $appends = [
        'product_image'
    ];

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function media(){
        return $this->morphMany(Media::class, 'mediable');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function getProductImageAttribute(){
        $coverImg = $this->media()->where('mediable_id', $this->id)->where('width', '>=', 50)->orderBy('width', 'asc')->pluck('name')->first();
        return '/storage/cover_images/'. $coverImg;
    }

    public function cover_image(){
        $coverImg = $this->media()->where('mediable_id', $this->id)->pluck('name')->first();
        return '/storage/cover_images/'. $coverImg;
    }

    public function getUrl($width){
        $coverImg = $this->media()->where('mediable_id', $this->id)->where('width', '>=', $width)->orderBy('width', 'asc')->pluck('name')->first();
        return '/storage/cover_images/'. $coverImg;
    }
}
