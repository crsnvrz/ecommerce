@extends('backend.layouts.app')

@section('content')
<div class="row">
    <div class="col-7">
        <div class="card">
            <div class="card-header">Product Categories</div>
            <div class="card-body">
                <div class="row">
                    <div v-for="category in categories" class="col-4">     
                        <categories :category="category"></categories>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="card">
            <div class="card-header">Products</div>
            <div class="card-body">
                <div class="row">
                    <div v-for="item in items" class="col-3">     
                        <products :product="item"></products>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="col-5">
        <div class="card">
            <div class="card-header">Orders</div>
            <div class="card-body">
                <table class="table" v-if="orders.length != 0">
                    <thead>
                        <tr>
                            <td>Product Name</td>
                            <td>Quantity</td>
                            <td>Price</td>
                            <td>Amount</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(value, key) in orders">
                            <td>
                                @{{ value.name }}
                            </td>
                            <td>
                                @{{ value.order_quantity }}
                            </td>
                            <td>
                                @{{ value.order_price.toFixed(2) }}
                            </td>
                            <td>
                                @{{  value.order_amount.toFixed(2)  }}
                            </td>
                            <td>
                            <button type="button" class="btn btn-danger" @click="removeOrder(key)">Remove</button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Total</b></td>
                            <td colspan="2">@{{ (total_amount - total_amount * 0.12).toFixed(2) }}</td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>12% VAT</b></td>
                            <td colspan="2">@{{ (total_amount * 0.12).toFixed(2) }}</td>
                        </tr>
                        <tr v-if="is_discount">
                            <td colspan="3"><b>20% Discount</b></td>
                            <td colspan="2">
                                @{{ (total_amount * 0.2).toFixed(2) }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Total Amount</b></td>
                            <td colspan="2">@{{ final_amount.toFixed(2) }}</td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Cash</b></td>
                            <td colspan="2">
                                <input type="text" id="cash" class="form-control" v-model="cash" v-on:focusout="calcChange" />
                            </td>
                        </tr>
                        <tr v-if="is_change">
                            <td colspan="3"><b>Change</b></td>
                            <td colspan="2">
                                @{{  change.toFixed(2)  }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" v-on:click="addDiscount">
                                    <label class="form-check-label" for="exampleCheck1">w/ Discount</label>
                                </div>
                                <button class="btn btn-success pull-right" @click="checkout($event)">Checkout</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div> 
    </div>
</div>
@include('backend.pages.cashier.modal')
@endsection

@section('js')
<script>
    transact = {
        branch_id: {{ Auth::user()->id }}
    }
</script>
<script src="{{ asset('js/cashier.js') }}"></script>
@endsection
