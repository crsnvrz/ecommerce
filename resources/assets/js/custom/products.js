

var a = new Vue({
    el: '#app',
    data: {
        is_show: false,
    },
    created (){
        this.initCat()
    },
    methods:{
        selectCategory(event){
            let sel = $(event.currentTarget).val()

            axios.post('/api/getCategory', {id: sel} )
                .then(response => {
                if(response.data.with_sizes){
                    this.is_show = true
                    return false
                }
                this.is_show = false
            })
        },
        initCat(){
            let sel = $("#product_category_id").val()

            axios.post('/api/getCategory', {id: sel} )
                .then(response => {
                if(response.data.with_sizes){
                    this.is_show = true
                    return false
                }
                this.is_show = false
            })
        },
        chooseCategory(){
            var cat = $("#categories option:selected").val()
            $("input[name='category_id']").val(cat)
            $("#prodForm").submit()
        }
    },
});

$(function(){
    a.selectCategory()
})