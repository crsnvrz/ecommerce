@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Edit Product</div>
    <div class="card-body">
            {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-row">
                <div class="form-group col-md-6">
                    {!! Form::label('name', 'Product Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('product_category_id', 'Product Category') !!}
                    {!! Form::select('product_category_id', $categories, null, ['class' => 'form-control category_select', 'v-on:change' => 'selectCategory', 'required']) !!}
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-4">
                    {!! Form::label('price', 'Regular Price') !!}
                    {!! Form::text('price', null, ['class' => 'form-control', 'required', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
                <div class="form-group col-4" v-if="is_show">
                    {!! Form::label('price_medium', 'Price (Medium)') !!}
                    {!! Form::text('price_medium', null, ['class' => 'form-control', 'required', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
                <div class="form-group col-4" v-if="is_show">
                    {!! Form::label('price_large', 'Price (Large)') !!}
                    {!! Form::text('price_large', null, ['class' => 'form-control', 'required', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
            </div>
            <div class="form-group">
                {{Form::file('cover_image')}}
            </div>
            {{Form::hidden('_method','PUT')}}
            <button type="submit" class ="btn btn-primary" onclick="blur()">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
</div> 
@endsection 
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('js/products.js') }}"></script>
@endsection